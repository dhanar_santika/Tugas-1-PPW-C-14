"""tugas1ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic import RedirectView
import fitur_stats.urls as stats
import fitur_addfriends.urls as friends
import fitur_updStatus.urls as upd
import fitur_profil.urls as fitur_profil

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^stats/', include(stats,namespace='stats')),
    url(r'^friends/', include(friends, namespace='friends')),
    url(r'^update-status/', include(upd, namespace='update-status')),
    url(r'^profil/', include(fitur_profil,namespace='profil')),
    url(r'^$', RedirectView.as_view(url="update-status/", permanent="false"), name='index'),
]
