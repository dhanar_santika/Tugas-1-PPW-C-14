from django.test import TestCase,Client
from django.http import HttpRequest
from django.urls import resolve
from .views import index
from .models import Friends

# Create your tests here.
class addfriend(TestCase):

    def test_add_friend_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):  
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_activity(self):
        addfriend = Friends.objects.create(name="Dengklek" ,url="dengklek.herokuapp.com")
        counting_friends = Friends.objects.all().count()
        self.assertEqual(counting_friends,1)



