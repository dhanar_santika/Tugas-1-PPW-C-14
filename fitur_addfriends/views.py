from django.shortcuts import render
from .models import Friends



def index(request):
	htmlpage = "friend.html"
	title = "friend"
	response = {"title":title}
	lstFriends = Friends.objects.all()
	response["friends"] = lstFriends

	if request.method == "POST":
		name = request.POST.get("nama","")
		url = request.POST.get("url","")
		friend = Friends(name=name,url=url)
		friend.save()
		response["pesan"] = "data berhasil disimpan"
	
	return render(request,htmlpage,response)	
	
