from django.test import TestCase,Client
from django.http import HttpRequest
from django.urls import resolve
from .views import index
from fitur_addfriends.models import Friends
from fitur_updStatus.models import Status

# Create your tests here.
class StatsUnitTest(TestCase):
    
    def test_stats_page_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_stats_page_title_is_stats(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title> Stats </title>', html_response)

    def test_stats_number_of_friends(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        number_of_friends = Friends.objects.all().count()
        self.assertIn('<h3>Friends : '+ str(number_of_friends) +' people</h3>', html_response)

    def test_stats_number_of_posts(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        number_of_posts = Status.objects.all().count()
        self.assertIn('<h3>Posts : '+ str(number_of_posts) +' posts</h3>', html_response)

    def test_latest_post(self):
        post = Status.objects.create(content="Test Post")
        
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        latest_post = Status.objects.all().reverse()[0].content

        self.assertIn("<h3>Latest Post</h3>", html_response)
        self.assertIn("<h4>"+latest_post+"</h4>", html_response)