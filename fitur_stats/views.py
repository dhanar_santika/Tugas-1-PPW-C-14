from django.shortcuts import render
from fitur_addfriends.models import Friends
from fitur_updStatus.models import Status

# Create your views here.
response = {}
def index(request):
    feeds = Status.objects.all().count()
    html = 'stats/stats.html'
    response['title'] = 'Stats'
    response['friends'] = Friends.objects.all().count()
    response['posts'] = feeds
    if(feeds!=0):
        latestFeed = Status.objects.get(pk=Status.objects.all().count())
        response['latest_post']= latestFeed.content
        response['latest_date']= latestFeed.created_date
    response['username'] = 'Hepzibah Smith'
    return render(request,html,response)