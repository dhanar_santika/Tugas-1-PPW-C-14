from django.db import models

# Create your models here.
class Status(models.Model):
	content = models.TextField()
	created_date = models.DateTimeField(auto_now_add = True)