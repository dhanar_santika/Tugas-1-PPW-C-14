from django.shortcuts import render
from .models import Status
from .forms import Status_Form
from django.http import HttpResponseRedirect

response = {}

def index(request):
    html = 'update.html'
    status = Status.objects.all()[::-1]
    response['status_form'] = Status_Form
    response['status'] = status
    response['title'] = 'Update Status'

    return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	response['title'] = 'Update Status'
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		if(len(response['status']) > 0 and len(response['status']) <= 160):
			status = Status(content=response['status'])
			status.save()
			return HttpResponseRedirect('/update-status/')
		
	return HttpResponseRedirect('/update-status/')
