from django import forms

class Status_Form(forms.Form):
	status_attrs = {
		'type' : 'text',
		'placeholder' : "What's on your mind?"
	}

	status = forms.CharField(label='', required=True, max_length=160, widget=forms.TextInput(attrs=status_attrs))
