from django.test import TestCase, Client
from .models import Status

# Create your tests here.

class update_status_unit_test(TestCase):

	def test_update_status_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code,200)

	def test_model_cant_create_new_status(self):
		status = 100*'Kappa'
		response = self.client.post('/update-status/add_status/', data={'created_date': '2017-10-12T14:14', 'status' : status})
		self.assertEqual(Status.objects.all().count(), 0)

	def test_model_can_create_new_status(self):
		status = 'Kappa123'
		response = self.client.post('/update-status/add_status/', data={'created_date': '2017-10-12T14:14', 'status' : status})
		self.assertEqual(Status.objects.all().count(), 1)

	def test_model_cant_create_empty_status(self):
		status = ''
		response = self.client.post('/update-status/add_status/', data={'created_date': '2017-10-12T14:14', 'status' : status})
		self.assertEqual(Status.objects.all().count(), 0);