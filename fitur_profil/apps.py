from django.apps import AppConfig


class FiturProfilConfig(AppConfig):
    name = 'fitur_profil'
