from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, bio_dict
from django.http import HttpRequest
from unittest import skip

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
        found = resolve('/profil/')
        self.assertEqual(found.func, index)

    def test_profile_bio_dict(self):
        # Check whether bio_dict is not None Object
        self.assertIsNotNone(bio_dict)

        # Check whether bio_dict entry is less than 3 items
        self.assertTrue(len(bio_dict) >= 3)

        # Checking all element in family member
        for bio in bio_dict:
            # Name cannot be None
            self.assertIsNotNone(bio['subject'])
            self.assertIsNotNone(bio['value'])

            # Name cannot be integer
            self.assertIsNot(type(bio['value']), type(8))

