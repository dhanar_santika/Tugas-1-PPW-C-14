from django.shortcuts import render

#Membuat isi dari fitur_profil
name = "Hepzibah Smith"
jenisexpertise = ["Marketing","Collector","Public Speaking"]
bio_dict = [
{'subject' : 'Birthday', 'value' : '10 Oktober 1998'},\
{'subject' : 'Gender', 'value' : 'Male'},\
{'subject' : 'Expertise', 'value' : jenisexpertise},\
{'subject' : 'Description', 'value' : 'Antique expert, Experience as marketer for 10 years'},\
{'subject' : 'Email', 'value' : 'HSmith@gmail.com'},\
]

def index(request):
    response = {'name': name, 'bio_dict': bio_dict, 'title' : 'profil'}
    return render(request, 'fitur_profil/fitur_profil.html', response)
